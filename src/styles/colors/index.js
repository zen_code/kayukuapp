export default {
  primary : "#FFDEBD",
  secondary : "#A7723E",
  primarydark : "#663300",
  white: "#ffffff",
  black: "#000000",
  green: "#00b300",
  lightBlack: "#484848",
  gray: "#F5F7F8",
  graydark: "#DEDFDF",
  grayprimary: "#F2F2F2",
  red: "#FF0000",
  accent: "#E74424",
};
