import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    display: "flex",
    alignItems: 'center',
    height: '25%',
    width: '20%',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 0,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubKategori extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
          <Image
            source={this.props.imageUri}
            style={{ width: 35, height: 35, resizeMode: "contain", marginTop: 5, marginBottom: 2 }}
          />
        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 3, alignItems: "center" }}>
            <Text style={{fontSize:8, fontWeight: "bold", alignItems:"center"}}>{this.props.name}</Text>
          </View>
      </View>
    );
  }
}


SubKategori.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
