import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    display: "flex",
    width: '100%',
    height:250,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    paddingBottom:5,
    backgroundColor:colors.white,
    borderRadius:10
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubProdukShop extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <View style={{width:'100%', height:180, alignItems: 'center', justifyContent: 'center',}}>
          <Image
            source={this.props.imageUri}
            style={{ width: '100%', height:'100%', marginTop: 0, marginBottom: 2, borderTopRightRadius:10, borderTopLeftRadius:10, }}
          />
        </View>
        <View style={{ flex: 1, width:'100%', paddingTop: 3, paddingLeft:10, paddingRight:10}}>
            <Text style={{fontSize:12, color:colors.black, fontWeight: "bold", }}>{this.props.name.substring(0, 20)}</Text>
            <Text style={{fontSize:12, paddingTop:5, fontWeight:'bold', color:colors.secondary}}>Rp{this.props.harga}</Text>
        </View>
        <View style={{ flex: 1, width:'100%', flexDirection: 'row', marginTop:25, paddingLeft:10, paddingRight:10, borderBottomRightRadius:10, borderBottomLeftRadius:10,}}>
            <View  style={{width:'50%',flexDirection:'row'}}>
                <Text style={{fontSize:10}}>0 Sold</Text>
            </View>
            <View style={{width:'50%'}}>

            </View>
        </View>
      </View>
    );
  }
}


SubProdukShop.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
