import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    height: 150,
    width: 120,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 0,
    borderRadius:10,
    backgroundColor:colors.white,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubFlashSale extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <View style={{width:120, height:120}}>
          <Image
            source={this.props.imageUri}
            style={{ width: 120, height:'100%', marginTop: 0, marginBottom: 2, borderTopRightRadius:10, borderTopLeftRadius:10, }}
          />
        </View>
        <View style={{ flex: 1, width:120, borderBottomRightRadius:10, borderBottomLeftRadius:10, backgroundColor:colors.gray, paddingTop: 3, alignItems: 'center'}}>
            <Text style={{fontSize:11, fontWeight: "bold"}}>{this.props.name}</Text>
        </View>
      </View>
    );
  }
}


SubFlashSale.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
