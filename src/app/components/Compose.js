import React from 'react';
import { View, StyleSheet, Text, Keyboard, TextInput } from 'react-native';
import { Button } from 'native-base'
import colors from '../../styles/colors';

class Compose extends React.Component {
	constructor(props) {
		super(props);
		this.submit = this.submit.bind(this);
	}

	//Membuat state text
	state = {
		text: ''
	}

	//submit method yang berfungsi ketika tombol ditekan
	submit() {
		//mengirim nilai ke database berdasarkan state text yang diterima
		this.props.submit(this.state.text);
		//mengatur state text menjadi kosong
		this.setState({
			text: ''
		})
		//menutup keyboard
		Keyboard.dismiss();
	}

	render(){
		return (
			<View style={styles.compose}>
				<TextInput
					style={styles.composeText}
					value={this.state.text}
					onChangeText={(text) => this.setState({text})}
					onSubmitEditing={(event) => this.submit()}
					editable = {true}
					maxLength = {40}
				/>
				<Button
					onPress={this.submit}
					style={styles.btn}
				>
				<Text style={styles.btnText}>SEND</Text>
				</Button>
			</View>
		)
	}
}

const styles = StyleSheet.create({
  composeText: {
    width: '80%',
		borderRadius:10,
    paddingHorizontal: 10,
    height: 40,
    backgroundColor: colors.white,
    borderColor: colors.gray,
    borderStyle: 'solid',
    borderWidth: 1,
  },
  compose: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
		backgroundColor:colors.gray,
		paddingBottom:5,
		paddingTop:5,
		paddingLeft:10,
		paddingRight:10
  },
	btn: {
		borderRadius:10,
		backgroundColor:colors.primarydark,
		width:60,
		height:40,
		alignItems:'center',
		justifyContent:'center'
	},
	btnText: {
		fontSize:13,
		color:colors.white
	},
});

export default Compose;
