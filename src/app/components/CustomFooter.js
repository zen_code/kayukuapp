import React, { Component } from "react";
import { Platform,Image,AsyncStorage } from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View, Badge } from "native-base";

import styles from "../screens/styles/CustomFooter";
import colors from "../../styles/colors";

class CustomFooter extends Component {
  constructor(props) {
    super(props);
    this.state={
      notif:2
    }
  }

  componentDidMount() {

  }

  render() {

    return (
      <View>
      {((this.props.menu != "APD")&&(this.props.menu != "Safety")&&(this.props.menu != "FireSystem")) && (<Footer>

        <FooterTab style={{ backgroundColor: "#ebebeb"}}>
        {this.props.menu == "Home" ? (
          <Button
            vertical
            onPress={() => this.props.navigation.navigate("HomeMenu")}
          >
              <Image
                source={require("../../assets/images/Home-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: colors.secondary, fontSize:10}}>Home</Text>
          </Button>
        ):(
          <Button
            vertical
            onPress={() => this.props.navigation.navigate("HomeMenu")}
          >
              <Image
                source={require("../../assets/images/Home.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Home</Text>
          </Button>
        )}


          {this.props.menu == "Favorite" ? (
            <Button
              vertical
              // active
              onPress={() => this.props.navigation.navigate("Favorite")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >
              <Image
                source={require("../../assets/images/Favorite-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: colors.secondary, fontSize:10}}>Favorite</Text>
            </Button>
          ) : (
            <Button
              vertical
              onPress={() => this.props.navigation.navigate("Favorite")}
            >
              <Image
                source={require("../../assets/images/Favorite.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Favorite</Text>
            </Button>
          )}

          {this.props.menu == "Chatting" ? (
            <Button badge
              vertical

              onPress={() => this.props.navigation.navigate("ChatHeader")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >
            {this.state.notif!=0 &&(
              <Badge style={{ position: "absolute", left: '50%', top: '10%', backgroundColor:colors.secondary }}><Text>{this.state.notif}</Text></Badge>
            )}

              <Image
                source={require("../../assets/images/Chat-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />

              <Text style={{  color: colors.secondary, fontSize:10 }}>Chatting</Text>

            </Button>

          ) : (
            <Button badge
              vertical
              onPress={() => this.props.navigation.navigate("ChatHeader")}
            >
            {this.state.notif!=0 &&(
              <Badge style={{ position: "absolute", left: '50%', top: '10%', backgroundColor:colors.secondary}}><Text>{this.state.notif}</Text></Badge>
            )}

              <Image
                source={require("../../assets/images/Chat.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Chatting</Text>
            </Button>
          )}

          {this.props.menu == "Account" ? (
            <Button
              vertical
              // active
              onPress={() => this.props.navigation.navigate("Account")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >
              <Image
                source={require("../../assets/images/Profil-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{  color: colors.secondary, fontSize:10 }}>Account</Text>
            </Button>
          ) : (
            <Button
              vertical
              onPress={() => this.props.navigation.navigate("Account")}
            >
              <Image
                source={require("../../assets/images/Profil.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Account</Text>
            </Button>
          )}


        </FooterTab>
      </Footer>)}

      </View>


    );
  }
}

export default CustomFooter;
