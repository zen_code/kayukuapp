import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Textarea
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import SubProduk from "../../components/SubProduk";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import Moment from 'moment'

var that;
class ListItemUlasan extends React.PureComponent {
  render() {
    return (
      <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:5, paddingRight:5}}>
        <View style={{flexDirection:'row', marginBottom:5, marginTop:5}}>
          <View style={{width:'18%', justifyContent: "center"}}>
            <View style={{width:50, height:50, borderRadius:50}}>
              {this.props.data.image=='null'?(
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    borderRadius:50
                  }}
                  source={require("../../../assets/images/Profil.png")}/>
              ):(
              <Image
                source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image }}
                style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:50 }}
              />)}
            </View>
          </View>
          <View style={{width:'82%'}}>
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'70%'}}>
                <Text style={{fontSize:11, color:colors.black, fontWeight:'bold'}}>{this.props.data.customers_name}</Text>
              </View>
              <View style={{width:'30%'}}>
                <Text style={{fontSize:9, textAlign:'right'}}>{this.props.data.date}</Text>
              </View>
            </View>
            <Text style={{fontSize:10}}>{this.props.data.ulasan}</Text>
          </View>
        </View>
        <View style={{borderColor:colors.graydark, borderBottomWidth:0.5, height:1, width:'100%'}}>
        </View>
      </View>
    );
  }
}

class ListItem extends React.PureComponent {
  navigateToScreen(route, listProduk) {
    AsyncStorage.setItem("listProduk", JSON.stringify(listProduk)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render() {
    return (
      <View style={{width:'50%', paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailProduk", this.props.data)}>
        <SubProduk
          imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image_primary }}
          name={this.props.data.name}
          harga={this.props.data.harga}
          terjual={this.props.data.stok}
          favorite={this.props.data.berat}
        />
      </TouchableOpacity>
      </View>
    );
  }
}

export default class DetailShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      idToko:'',
      listShop:[],
      listProduk:[],
      visibleRating: false,
      visibleVoucher: false,
      visibleHelp: false,
      jumlahProdukShop:'',
      showUlasan:false,
      customers_id:'',
      customers_image:'',
      customers_name:'',
      listUlasan:[],
      ulasan:'',
      dateNow:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profil").then(dataUser => {
        this.setState({
          customers_id: JSON.parse(dataUser).id,
          customers_name: JSON.parse(dataUser).name,
          customers_image: JSON.parse(dataUser).image,
        })
    })
    AsyncStorage.getItem('idToko').then((idToko) => {
      var dateNow = Moment().format('YYYY-MM-DD');
      this.setState({
        idToko: idToko,
        dateNow:dateNow,
      });
      this.loadData();
      this.loadProduk();
      this.loadUlasan();
    })
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadData();
      this.loadProduk();
      this.loadUlasan();
    });
  }

  loadData() {
    this.setState({ isLoading: true });
    var url = GlobalConfig.SERVERHOST + 'get_toko_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((responseJson) => {
        //alert(JSON.stringify(responseJson.data.filter(x => x.id == this.state.idToko)))
          this.setState({
                listShop: responseJson.data.filter(x => x.id == this.state.idToko),
                isLoading: false
            });
      })
      .catch((error) => {
          console.log(error)
      })
  }

  loadUlasan() {
    var url = GlobalConfig.SERVERHOST + 'getUlasan';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((responseJson) => {
          this.setState({
                listUlasan: responseJson.data.filter(x => x.toko_id == this.state.idToko),
                isLoading: false
            });
      })
      .catch((error) => {
          console.log(error)
      })
  }

  loadProduk(){
    var url = GlobalConfig.SERVERHOST + 'get_product_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listProduk: responseJson.data.filter(x => x.toko_id == this.state.idToko),
                  jumlahProdukShop: responseJson.data.filter(x => x.toko_id == this.state.idToko).length,
              });
          }
      })
  }
  send(){
    if(this.state.ulasan==''){
      Alert.alert('Information', 'Input Review', [{
        text: 'OK'
      }]);
    } else {
      Alert.alert(
        'Confirmation',
        'Do you want to SEND review?',
        [
          { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'Yes', onPress: () => this.sendFinal() },
        ],
        { cancelable: false }
      );
    }
  }
  sendFinal(){
    var url = GlobalConfig.SERVERHOST + 'addUlasan';
    var formData = new FormData();
    formData.append("customers_id", this.state.customers_id)
    formData.append("customers_name", this.state.customers_name)
    formData.append("toko_id", this.state.idToko)
    formData.append("date", this.state.dateNow)
    formData.append("ulasan", this.state.ulasan)
    formData.append("image", this.state.customers_image)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200) {
          Alert.alert('Success', 'Review Product Success', [{
            text: 'OK'
          }]);
          this.loadUlasan()
          this.setState({
            ulasan:''
          })
        } else{
          Alert.alert('Error', 'Review Product Failed', [{
            text: 'OK'
          }]);
        }
    });
  }


  navigateToScreen(route, listShop) {
    AsyncStorage.setItem("listShop", JSON.stringify(listShop)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  loadRating(){
    this.setState({
      visibleRating: true
    });
  }

  loadBalance(){
    Alert.alert(
      'Information',
      'Under Construction',
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  loadIncome(){
    Alert.alert(
      'Information',
      'Under Construction',
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  loadHelp(){
    this.setState({
      visibleHelp: true
    });
  }

  showUlasan(){
    this.setState({
      showUlasan: !this.state.showUlasan
    })
  }

  _renderItem = ({ item }) => <ListItem data={item} />;
  _renderItemUlasan = ({ item }) => <ListItemUlasan data={item} />;

  render() {
    that=this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>):(
      <Container style={styles.wrapper}>
      <ScrollView>
        <Content style={{ marginTop:0, backgroundColor: colors.gray}}>
          <View style={{width: '100%', height:150, paddingRight:10, paddingLeft:10}}>
            <Image
              style={{
                alignSelf: "center",
                width: '100%',
                height: '100%',
                borderBottomRightRadius:50,
                borderBottomLeftRadius:50
              }}
              source={require("../../../assets/images/produk/produk2.jpg")}>
            </Image>
            <TouchableOpacity
              transparent
              style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
              onPress={()=>this.props.navigation.navigate("HomeMenu")}
            >
              <Icon
                name='arrow-back'
                size={10}
                style={{color:colors.white, fontSize:20}}
              />
            </TouchableOpacity>
          </View>
          <Text>{this.state.listShop.name}</Text>
          {this.state.listShop.map((listShop, index) => (
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{borderRadius:10, backgroundColor:colors.white, flex:1, flexDirection:'row'}}>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5}}>
                <View style={{width:'35%'}}>
                  <Image
                    style={{
                      width: 50,
                      height: 50,
                      borderRadius:50
                    }}
                    source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listShop.image }}/>
                </View>
                <View style={{width:'65%'}}>
                  <Text style={{fontSize:12, paddingTop:0, fontWeight:'bold', color:colors.black}}>{listShop.name}</Text>
                  <Text style={{fontSize:10, paddingTop:0}}>Active 5 minutes ago</Text>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Icon
                        name='ios-pin'
                        style={{fontSize:15, color:colors.black, paddingRight:5}}
                    />
                    <Text style={{fontSize:12, paddingTop:0}}>{listShop.alamat}</Text>
                  </View>
                </View>
              </View>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5}}>
                <View style={{width:'50%', justifyContent: "center", alignItems: "center"}}>
                  <Text style={{fontSize:15, color:colors.black, fontWeight:'bold'}}>{this.state.jumlahProdukShop}</Text>
                  <Text style={{fontSize:10}}>Products</Text>
                </View>
                <View style={{width:'50%', justifyContent: "center", alignItems: "center"}}>
                  <Text style={{fontSize:15, color:colors.black, fontWeight:'bold'}}>0</Text>
                  <Text style={{fontSize:10}}>Rating</Text>
                </View>
              </View>
            </View>
          </View>
          ))}


          <View style={{width:'100%', paddingLeft:5, paddingRight:5, marginTop:10}}>
            <FlatList
              data={this.state.listProduk}
              renderItem={this._renderItem}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={{width:'100%', paddingLeft:5, paddingRight:5, marginTop:10, paddingBottom:5, alignItems:'center', justifyContent:'center'}}>
            <View style={{flex:1, flexDirection:'row'}}>
            <View>
            <TouchableOpacity
              transparent
              onPress={() => this.showUlasan()}>
            <Text style={{fontSize:12}}>Review</Text>
            </TouchableOpacity>
            </View>
            <View style={{paddingLeft:5}}>
            <TouchableOpacity
              transparent
              onPress={() => this.showUlasan()}>
              {this.state.showUlasan!=true ? (
                <Icon
                    name='arrow-dropup'
                    style={{fontSize:30, color:colors.secondary, paddingTop:-7, marginTop:-7}}
                />
              ):(
                <Icon
                    name='arrow-dropdown'
                    style={{fontSize:30, color:colors.secondary, paddingTop:-7, marginTop:-7}}
                />
              )}
            </TouchableOpacity>
            </View>
            </View>
          </View>
          {this.state.showUlasan==true && (
            <View>
            <View style={{width:'100%', paddingLeft:5, paddingRight:5, marginTop:2, paddingBottom:10, alignItems:'center', justifyContent:'center'}}>
              <FlatList
                data={this.state.listUlasan}
                renderItem={this._renderItemUlasan}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={{width:'100%', paddingLeft:5, paddingRight:5, marginTop:2, paddingBottom:5, alignItems:'center', justifyContent:'center'}}>
            <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:5, paddingRight:5}}>
              <View style={{flexDirection:'row', marginBottom:5, marginTop:5}}>
                <View style={{width:'18%', justifyContent: "center"}}>
                  <View style={{width:50, height:50, borderRadius:50}}>
                    {this.state.customers_image==null?(
                      <Image
                        style={{
                            width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:50
                        }}
                        source={require("../../../assets/images/Profil.png")}/>
                    ):(
                      <Image
                        source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.state.customers_image }}
                        style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:50 }}
                      />)}
                  </View>
                </View>
                <View style={{width:'82%'}}>
                    <View style={{width:'100%'}}>
                      <Text style={{fontSize:11, color:colors.black, fontWeight:'bold'}}>{this.state.customers_name}</Text>
                    </View>
                    <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{width:'75%'}}>
                        <Textarea style={{borderRadius:20, fontSize:10}} rowSpan={1.5} bordered value={this.state.ulasan} placeholder='Type Review ...' onChangeText={(text) => this.setState({ ulasan: text })} />
                      </View>
                      <View style={{width:'25%'}}>
                        <View style={{backgroundColor:colors.secondary, height:35, borderRadius:20, marginTop:5, marginLeft:5, alignItems:'center', justifyContent:'center'}}>
                          <TouchableOpacity
                            transparent
                            onPress={() => this.send()}>
                            <Text style={{fontSize:11, color:colors.white}}>Send</Text>
                          </TouchableOpacity>
                        </View>
                      </View>

                    </View>
                </View>
              </View>
              </View>
            </View>
            </View>
          )}
        </Content>
      </ScrollView>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleRating}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleRating: false });
            }}
            dialogTitle={<DialogTitle title="My Rating" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5}}>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#DEDFDF'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#DEDFDF'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#DEDFDF'}}
                    />
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleVoucher}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleVoucher: false });
            }}
            dialogTitle={<DialogTitle title="My Vouchers" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5, alignItems:'center'}}>
                    <Text style={{fontSize:30, fontWeight:'bold', textAlign:'center'}}>RP. 0,-</Text>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

        <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleHelp}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog }}
              onTouchOutside={() => {
                this.setState({ visibleHelp: false });
              }}
              dialogTitle={<DialogTitle title="Help Centre" />}
              actions={[
                <DialogButton
                  style={{
                    fontSize: 11,
                    backgroundColor: colors.white,
                    borderColor: colors.blue01
                  }}
                  text="SORT"
                  onPress={() => this.loadData()}
                  // onPress={() => this.setState({ visibleFilter: false })}
                />
              ]}
            >
              <DialogContent>
                {
                  <View style={{flexDirection:'row', flex:1, paddingTop:5, alignItems:'center'}}>
                      <Text style={{fontSize:20, fontWeight:'bold', textAlign:'center'}}>official@kayuku.id</Text>
                  </View>
                }
              </DialogContent>
            </Dialog>
          </View>
      <CustomFooter navigation={this.props.navigation} menu="Home" />
      </Container>
    );
    }
}
