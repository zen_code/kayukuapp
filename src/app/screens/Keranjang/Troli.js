import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Thumbnail
} from "native-base";
import styles from "./../styles/Troli";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../../components/SubProduk";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import CheckBox from 'react-native-check-box';
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Troli extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      customers_id:'',
      listTroli:[],
      jumlahOrder:[1,1,1,1,1,1,1,1,1,1,1,1,11,1,1,1,1,1,1,1,1,1,1,1,1],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profil").then(dataUser => {
        this.setState({
          customers_id: JSON.parse(dataUser).id,
        });
        this.loadTroli()
    });
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadTroli();
    });
  }

  loadTroli(){
    this.setState({ isLoading: true });
    var url = GlobalConfig.SERVERHOST + 'list_order';

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                listTroli: responseJson.data.filter(x => x.customers_id == this.state.customers_id && x.status == 'troli'),
                isLoading: false
              });
          }

      })
  }

  konfirmasiDeleteTrolli(id){
    Alert.alert(
      'Confirmation',
      'Do you want to DELETE this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.delete(id) },
      ],
      { cancelable: false }
    );
  }

  delete(id){
    var url = GlobalConfig.SERVERHOST + 'deleteOrder';
    var formData = new FormData();
    formData.append("id", id)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
            Alert.alert('Success', 'Delete Product Success', [{
              text: 'OK'
            }]);
            this.componentDidMount()
          } else{
            Alert.alert('Error', 'Delete Products Failed', [{
              text: 'OK'
            }]);
          }
      })
  }

  ubahJumlah(jumlah, index) {
    let arr = this.state.jumlahOrder;
    arr[index] = jumlah;
    this.setState({
      jumlahOrder: arr
    });
  }

  tambahJumlah(index) {
    let arr = this.state.jumlahOrder;

    arr[index] = arr[index] + 1;
    this.setState({
      jumlahOrder: arr
    });

    console.log(this.state.jumlahOrder[index]);
  }

  kurangJumlah(index) {
    let arr = this.state.jumlahOrder;
    if (arr[index] == 1 || arr[index] == null || arr[index] == undefined) {
    } else {
      arr[index] = arr[index] - 1;
      this.setState({
        jumlahOrder: arr
      });
    }
  }

  navigateToScreen(route, detailOrder) {
    AsyncStorage.setItem("detailOrder", JSON.stringify(detailOrder)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    let arr = this.state.jumlahOrder;
    var listOrder;
    if (this.state.isLoading) {
      listOrder = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        if (this.state.listTroli == '') {
          listOrder = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Data!</Text>
            </View>
          );
        } else {
      listOrder = this.state.listTroli.map((listOrder, index) => (
        <View key={index}>
        <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
          <View style={{flexDirection:'row', marginBottom:10, marginTop:10}}>
            <View style={{width:'25%', justifyContent: "center"}}>
              <View style={{width:75, height:75}}>
                <Image
                  source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listOrder.image }}
                  style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
                />
              </View>
            </View>
            <View style={{width:'70%'}}>
              <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{listOrder.name}</Text>
              <Text style={{fontSize:10, paddingTop:5, color:colors.primarydark, fontWeight:'bold'}}>RP {listOrder.harga}</Text>
              <View style={{flex:1, flexDirection:'row', marginTop:5}}>
                <View style={{width:'15%', height:30, borderWidth:1, borderColor:colors.graydark, justifyContent: "center", alignItems:'center', borderTopLeftRadius:20, borderBottomLeftRadius:20}}>
                  <TouchableOpacity
                    transparent
                    onPress={() => this.kurangJumlah(index)}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>-</Text>
                  </TouchableOpacity>
                </View>
                <View style={{width:'25%', height:30, borderTopWidth:1, borderBottomWidth:1, borderColor:colors.graydark, justifyContent: "center", alignItems:'center'}}>
                <Input
                  style={{
                    height:30,
                    marginTop:-5,
                    fontSize:12,
                    textAlign:'center'}}
                  value={this.state.jumlahOrder[index] + ""}
                  keyboardType='numeric'
                  onChangeText={text => this.ubahJumlah(text, index)}
                  />
                </View>
                <View style={{width:'15%', height:30, borderWidth:1, borderColor:colors.graydark, justifyContent: "center", alignItems:'center', borderTopRightRadius:20, borderBottomRightRadius:20}}>
                  <TouchableOpacity
                    transparent
                    onPress={() => this.tambahJumlah(index)}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>+</Text>
                  </TouchableOpacity>
                </View>
                <View style={{width:'35%', height:25, paddingLeft:10}}>
                  <View style={styles.btnCheckUot}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FormOrder", listOrder)}
                    >
                      <Text style={styles.textCheckOut}>BUY NOW</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            <View style={{width:'5%'}}>
              <TouchableOpacity
                transparent
                onPress={() => this.konfirmasiDeleteTrolli(listOrder.id)}>
                <Icon
                  name='ios-trash'
                  style={{fontSize:15}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </View>
      ))
      }
    }

    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.navigate("HomeMenu")}
        >
          <Icon
            name='arrow-back'
            style={{fontSize:20, color:colors.black}}
          />
        </Button>
        </Left>
        <Body style={{flex:3, alignItems:'center' }}>
          <Title style={{fontSize:18, color:colors.black}}>Troli</Title>
        </Body>
        <Right style={{ flex: 1}}>
          <Text style={{fontSize:12, fontWeight:'bold', color:colors.black}}>Delete All</Text>
        </Right>
      </Header>
      <View style={{backgroundColor:colors.white, paddingLeft:10, paddingTop:5, paddingRight:10, paddingBottom:10}}>
        <View style={{flexDirection:'row', marginBottom:5}}>
        <View style={{width:'90%', justifyContent: "center", paddingLeft:5}}>
          <Text style={{fontSize:12, fontWeight:'bold'}}>Choose Produk</Text>
        </View>
        </View>
      </View>
      <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
        {listOrder}
      </View>
      </Container>
      );
  }
}
