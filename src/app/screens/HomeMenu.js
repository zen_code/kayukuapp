import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import CustomFooter from "../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../components/SubProduk";
import SubFlashSale from "../components/SubFlashSale";
import Ripple from "react-native-material-ripple";
import loadProduk from "../data/allProduk.json";
import GlobalConfig from "../components/GlobalConfig";
import GlobalConfig2 from "../components/GlobalConfig2";
import GlobalConfig3 from "../components/GlobalConfig3";
import Moment from 'moment'

var that;
class ListItemShop extends React.PureComponent {
  navigateToScreen(route, idToko) {
    AsyncStorage.setItem("idToko", JSON.stringify(idToko)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:130, paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailShop", this.props.data.id)}>
        {this.props.data.image!=null ? (
          <SubFlashSale
            imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image }}
            name={this.props.data.name}
          />
        ):(
          <SubFlashSale
            imageUri={require("../../assets/images/nopic.png")}
            name={this.props.data.name}
          />
        )}
        </TouchableOpacity>
      </View>
    );
  }
}

class ListItem extends React.PureComponent {
  navigateToScreen(route, listProduk) {
    AsyncStorage.setItem("listProduk", JSON.stringify(listProduk)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'50%', paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailProduk", this.props.data)}>
        <SubProduk
          imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image_primary }}
          name={this.props.data.name}
          harga={this.props.data.harga}
          terjual={this.props.data.stok}
          favorite={this.props.data.berat}
        />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class HomeMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      search:"Search",
      listProduk:[],
      listShop:[],
      dataSource:[
        {
          title: 'KayuKu',
          caption: 'Marketplace',
          url: GlobalConfig3.SERVERHOST + "images/produk/produk8.jpg"
        },
        {
          title: 'App Kayuku',
          caption: 'Marketplace',
          url: GlobalConfig3.SERVERHOST + "images/produk/produk9.jpg"
        },
        {
          title: 'KayuKu',
          caption: 'Marketplace',
          url: GlobalConfig3.SERVERHOST + "images/produk/produk11.jpg"
        }
      ]
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.loadProduk()
    this.loadShop()
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadProduk()
      this.loadShop()
    });
  }

  componentWillMount(){
    this.setState({
      interval:setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        })
      }, 5000)
    })
  }

  componentWillUpdate(){
    clearInterval(this.state.interval)
  }

  loadProduk(){
    var url = GlobalConfig.SERVERHOST + 'get_product_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listProduk: responseJson.data,
                  isLoading: false
              });
          }
      })
  }

  loadShop(){
    var url = GlobalConfig.SERVERHOST + 'get_toko_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listShop: responseJson.data.filter(x => x.alamat !=null),
                  isLoading: false
              });
          }
      })
  }

  navigateToScreen(route, filter) {
    AsyncStorage.setItem("filter", (JSON.stringify(filter))).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  _renderItem = ({ item }) => <ListItem data={item} />;
  _renderItemShop = ({ item }) => <ListItemShop data={item} />;

  render() {
    that=this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 3 }}>
          <View style={styles.searchHeader}>
              <TextInput style={{height:40, marginLeft:10, marginRight:10}} value={this.state.search} onChangeText={(text) => this.setState({ search: text })} />
          </View>
        </Left>
        <Right style={{ flex: 2, marginLeft:15 }}>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("ScanToko")}
          >
            <Image
              source={require("../../assets/images/QR.png")}
              style={{ width: 35, height: 35, resizeMode: "contain" }}
            />
          </Button>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("Troli")}
          >
            <Image
              source={require("../../assets/images/Keranjang.png")}
              style={{ width: 30, height: 30, resizeMode: "contain" }}
            />
          </Button>
        </Right>
      </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <ScrollView>
          <View style={{width:'100%', marginTop:0, backgroundColor:colors.white}}>
            <View style={{marginTop:0}}>
            <Slideshow
              dataSource={this.state.dataSource}
              position={this.state.position}
              onPositionChanged={position => this.setState({ position})}
            />
            </View>
          </View>
          <View style={{width:'100%', marginTop:0, backgroundColor:colors.white}}>
            <View style={{marginTop:0}}>
              <View style={{flex:1, flexDirection:'row', paddingTop:20}}>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 1)}
                    >
                    <Image
                      source={require("../../assets/images/RuangTamu.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Living Room</Text>
                </View>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 2)}
                    >
                    <Image
                      source={require("../../assets/images/RuangMakan.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Dining Room</Text>
                </View>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 3)}
                    >
                    <Image
                      source={require("../../assets/images/KamarTidur.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Bedroom</Text>
                </View>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 4)}
                    >
                    <Image
                      source={require("../../assets/images/RuangKerja.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Workspace</Text>
                </View>
              </View>
              <View style={{flex:1, flexDirection:'row', paddingTop:10, paddingBottom:10}}>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 5)}
                    >
                    <Image
                      source={require("../../assets/images/Dapur.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Kitchen</Text>
                </View>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 6)}
                    >
                    <Image
                      source={require("../../assets/images/KamarMandi.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Bathroom</Text>
                </View>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 7)}
                    >
                    <Image
                      source={require("../../assets/images/Decorasi.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Decoration</Text>
                </View>
                <View style={{width:'25%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.navigateToScreen("FilterProduk", 8)}
                    >
                    <Image
                      source={require("../../assets/images/Koleksi.png")}
                      style={{ width: 40, height: 40, resizeMode: "contain" }}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Collection</Text>
                </View>
              </View>
            </View>
          </View>
          {/*
          <View style={styles.title}>
            <View style={{width:'50%'}}>
              <Text style={styles.titleTextLeft}>Populer</Text>
            </View>
            <View style={{width:'50%'}}>
              <Text style={styles.titleTextRight}>View All></Text>
            </View>
          </View>*/}

          <View style={styles.title}>
            <View style={{width:'50%'}}>
              <Text style={styles.titleTextLeft}>New Seller</Text>
            </View>
            <View style={{width:'50%'}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("AllShop")}
              >
              <Text style={styles.titleTextRight}>View All></Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{width:'100%', marginTop:0, paddingBottom:5, backgroundColor:colors.white}}>
            <View style={{marginTop:0}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.listShop}
                renderItem={this._renderItemShop}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
            </View>
          </View>

          <View style={styles.title}>
            <View style={{width:'50%'}}>
              <Text style={styles.titleTextLeft}>Poducts</Text>
            </View>
            <View style={{width:'50%'}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("AllProduk")}
              >
              <Text style={styles.titleTextRight}>View All></Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{width:'100%', marginTop:5, paddingRight:5}}>
            <View style={{marginTop:0}}>
              <FlatList
                data={this.state.listProduk}
                renderItem={this._renderItem}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>


          {/*<View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width:  }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>Daily Report</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }}  onPress={() => this.props.navigation.navigate("DailyReport")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.listShop}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>*/}


      </ScrollView>
      <CustomFooter navigation={this.props.navigation} menu="Home" />

      </Container>
      );
  }
}
