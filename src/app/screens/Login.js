import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      username:'',
      password:'',
      visibleLoading: false,
    };
  }

  static navigationOptions = {
    header: null
  };

  logOut(){
    Alert.alert(
      'Confirmation',
      'Logout KAYUKU Mobile?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.methLogOut() },
      ],
      { cancelable: false }
    );
  }

  methLogOut(){
    this.props.navigation.navigate("SplashScreen")
  }

  konfirmasiLogin(){
    if(this.state.username==""){
      Alert.alert(
        'Information',
        'Input Username',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password==""){
      Alert.alert(
        'Information',
        'Input Password',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {
      var url = GlobalConfig.SERVERHOST + 'login';
      var formData = new FormData();
      formData.append("username", this.state.username)
      formData.append("password", this.state.password)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((response) => {
            if(response.status == 200) {
              AsyncStorage.setItem('profil', JSON.stringify(response.data)).then(() => {
                // Alert.alert('Success', 'Login Success', [{
                //   text: 'OK'
                // }])
                this.props.navigation.navigate("HomeMenu");
              })
            } else if (response.status == 404) {
                Alert.alert(
                  'Login',
                  'Username Not Found',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
            } else {
                Alert.alert(
                  'Login',
                  'Password Salah',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
            }
        })
        .catch((error) => {
          Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
            text: 'Ok'
          }])
          console.log(error)
        })
    }
  }

  logOut(){
    this.props.navigation.navigate("SplashScreen")
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: 300,
                }}
                source={require("../../assets/images/head-img.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.logOut()}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 20, marginTop:-80, paddingBottom:40 }}>
              <View>
                <Text style={styles.signIn}>SIGN IN</Text>
              </View>
              <Form style={{ marginLeft: 0, marginRight:10, marginTop:5}}>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Username</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='contact'
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input returnKeyType='next' value={this.state.username} onChangeText={(text) => this.setState({ username: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Password</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='unlock'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input returnKeyType='go' secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
                  </View>
                </Item>
              </Form>
              </Card>
              <CardItem style={{ borderRadius: 0, marginTop:100}}>
                <View style={{ flex: 1, flexDirection:'column'}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 0,
                      backgroundColor: colors.primarydark,
                      borderRadius: 15
                    }}
                    onPress={() => this.konfirmasiLogin()}
                  >
                    <Text style={{color:colors.white}}>LOGIN</Text>
                  </Button>
                </View>
                <View>
                  <Text style={styles.weatherTextLink}>Forgot Password ?</Text>
                </View>
                </View>
              </CardItem>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}
