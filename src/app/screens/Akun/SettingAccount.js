import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";

import styles from "./../styles/Login";
import colors from "../../../styles/colors";
import CustomRadioButton from "../../components/CustomRadioButton";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import ImagePicker from "react-native-image-picker";

export default class SettingAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      isMale: true,
      isFemale: false,
      id:'',
      name:'',
      email:'',
      passwordValidasi:'',
      dataUser:'',
      dataUserId:'',
      pickedImage: '',
      pickedImageValidasi: '',
      uri: '',
      fileType:'',
      fileNameUpload: '',
    };
  }

  static navigationOptions = {
    header: null
  };


  componentDidMount() {
      AsyncStorage.getItem('dataUser').then((dataUser) => {
        this.setState({
          id: JSON.parse(dataUser).id,
          name: JSON.parse(dataUser).name,
          email: JSON.parse(dataUser).email,
          username: JSON.parse(dataUser).username,
          address: JSON.parse(dataUser).address,
          //image: JSON.parse(dataUser).image,
        });
        this.setState({
          pickedImage :  GlobalConfig2.SERVERHOST+'images/'+ JSON.parse(dataUser).image,
          pickedImageValidasi :  GlobalConfig2.SERVERHOST+'images/'+ JSON.parse(dataUser).image,
        })
      })
  }

  pickImageHandlerUpload = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
        if (res.didCancel) {
            console.log("User cancelled!");
        } else if (res.error) {
            console.log("Error", res.error);
        } else {
            this.setState({
                pickedImage: res.uri,
                uri: res.uri,
                fileType:res.type
            });

        }
    });
  }


  konfirmasiUpdate(){
    if(this.state.name==""){
      Alert.alert(
        'Information',
        'Input Full Name',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.email==""){
      Alert.alert(
        'Information',
        'Input Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.address==""){
      Alert.alert(
        'Information',
        'Input Address',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {
          var url = GlobalConfig.SERVERHOST + 'updateProfile';
          var formData = new FormData();
          formData.append("id", this.state.id)
          formData.append("name", this.state.name)
          formData.append("email", this.state.email)
          formData.append("address", this.state.address)
          if (this.state.uri!='') {
            formData.append("image", {
              uri: this.state.uri,
              type: this.state.fileType,
              name: 'imageuser_'+this.state.name
          });
          }
          fetch(url, {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
          }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.status == 200) {
                Alert.alert('Success', 'Update Success', [{
                  text: 'OK'
                }]);
                this.props.navigation.navigate("Account");
              } else{
                Alert.alert('Error', 'Update Failed', [{
                  text: 'OK'
                }]);
              }
          });
      }
  }

  render() {
    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff", width:'100%' }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: '100%',
                  height: 300,
                }}
                source={require("../../../assets/images/head-img.png")}>
              </ImageBackground>
              <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 20, marginTop:-250, paddingBottom:40 }}>
              <View>
                <Text style={styles.signIn}>UPDATE PROFILE</Text>
              </View>
              <Form style={{ marginLeft: 10, marginRight:20, marginTop:5}}>
                <View style={{alignItems:'center'}}>
                  <TouchableOpacity
                    transparent
                    onPress={this.pickImageHandlerUpload}
                  >
                    <View style={styles.placeholder}>
                        <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                    </View>
                  </TouchableOpacity>
                </View>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Full Name</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='contact'
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.name} onChangeText={(text) => this.setState({ name: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Email</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Username Kayuku</Label>
                  <View style={{flex:1, flexDirection:'row', paddingTop:5}}>
                    <View style={{width:'35%', alignItems: 'center', justifyContent: 'center', backgroundColor:colors.primary}}>
                        <Text style={{fontSize:12}}>Kayuku.com/</Text>
                    </View>
                    <View style={{width:'65%', borderWidth:1, borderColor:colors.primary}}>
                        <TextInput style={{height:40}} value={this.state.username} editable={false} onChangeText={(text) => this.setState({ username: text })} />
                    </View>
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Address</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='ios-pin'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                    <Input value={this.state.address} onChangeText={(text) => this.setState({ address: text })} />
                  </View>
                </Item>
              </Form>
              </Card>
              <CardItem style={{ borderRadius: 0, marginTop:10}}>
                <View style={{ flex: 1, flexDirection:'column'}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 10,
                      borderWidth: 0,
                      backgroundColor: colors.primarydark,
                      borderRadius: 15
                    }}
                    onPress={() => this.konfirmasiUpdate()}
                  >
                    <Text style={{color:colors.white}}>SAVE</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}
