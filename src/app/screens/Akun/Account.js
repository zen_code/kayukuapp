import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      dataUser:[],
      username:'',
      visibleRating: false,
      visibleVoucher: false,
      visibleHelp: false,
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.getItem("profil").then(dataUser => {
        this.setState({
          username: JSON.parse(dataUser).username,
          password: JSON.parse(dataUser).password,

        });
        this.loadData();
        //this.setState({ dataUser: JSON.parse(dataUser) });
        //this.loadData(JSON.parse(dataUser));
    });
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadData();
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
      this.props.navigation.goBack(null);
      return true;
  };

  navigateToScreen(route, dataUser) {
    AsyncStorage.setItem("dataUser", JSON.stringify(dataUser)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  loadData(){
    var url = GlobalConfig.SERVERHOST + 'validasiUser';
    var formData = new FormData();
    formData.append("username", this.state.username)
    formData.append("password", this.state.password)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((response) => {
          if(response.status == 200) {
              this.setState({ dataUser: response.data });
          }
      })
  }

  exitApp(){
    Alert.alert(
      'Confirmation',
      'Logout KAYUKU Mobile?',
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() }
      ],
      { cancelable: false }
    );
  }

  loadRating(){
    this.setState({
      visibleRating: true
    });
  }

  loadVoucher(){
    this.setState({
      visibleVoucher: true
    });
  }

  loadHelp(){
    this.setState({
      visibleHelp: true
    });
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <Content style={{ marginTop:0}}>
          <View style={{width: '100%', height:150, paddingRight:10, paddingLeft:10}}>
            <Image
              style={{
                alignSelf: "center",
                width: '100%',
                height: '100%',
                borderBottomRightRadius:50,
                borderBottomLeftRadius:50
              }}
              source={require("../../../assets/images/produk/produk2.jpg")}>
            </Image>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{borderRadius:10, borderWidth:1, borderColor:colors.graydark, flex:1, flexDirection:'row'}}>
              <View style={{width:'65%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5}}>
                <View style={{width:'30%'}}>
                  {this.state.dataUser.image==null ?(
                    <Image
                      style={{
                        width: 50,
                        height: 50,
                        borderRadius:50
                      }}
                      source={require("../../../assets/images/nopic.png")}>
                    </Image>
                  ):(
                    <Image
                      style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50
                      }}
                      source={{
                        uri:
                        GlobalConfig2.SERVERHOST + "images/" + this.state.dataUser.image}}/>
                  )}
                </View>
                <View style={{width:'70%'}}>
                  <Text style={{fontSize:12, paddingTop:0, fontWeight:'bold', color:colors.black}}>{this.state.dataUser.name}</Text>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Icon
                        name='ios-pin'
                        style={{fontSize:15, color:colors.black, paddingRight:5}}
                    />
                    {this.state.dataUser.address==null ?(
                      <Text style={{fontSize:12, paddingTop:0}}>Change Address</Text>
                    ):(
                      <Text style={{fontSize:12, paddingTop:0}}>{this.state.dataUser.address}</Text>
                    )}
                  </View>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                          name='mail-open'
                          style={{fontSize:15, color:colors.black, paddingRight:5}}
                      />
                    <Text style={{fontSize:12, paddingTop:0}}>{this.state.dataUser.email}</Text>
                  </View>
                </View>
              </View>

              <View style={{width:'35%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <View style={{width:'100%', height:30, marginRight:10, justifyContent: "center", alignItems: "center", backgroundColor:colors.secondary, borderRadius:30}}>
                <TouchableOpacity
                  transparent
                  onPress={() =>
                      this.navigateToScreen(
                        "MyShop",
                        this.state.dataUser
                      )
                    }
                >
                  <Text style={{fontSize:15, color:colors.white}}>My Shop</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{marginTop:10, borderWidth:1, borderRadius:10, borderColor:colors.graydark}}>
              <View style={{flex:1, flexDirection:'row', paddingTop:10}}>
                <View style={{width:'33.3%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, backgroundColor:'#319F4C', borderRadius:30, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("ToPay")}
                    >
                    <Icon
                        name='ios-star'
                        style={{fontSize:25, color:colors.white}}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>To Pay</Text>
                </View>
                <View style={{width:'33.3%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, backgroundColor:'#0D83DD', borderRadius:30, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("ToShip")}
                    >
                    <Icon
                        name='ios-cube'
                        style={{fontSize:25, color:colors.white}}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>To Ship</Text>
                </View>
                <View style={{width:'33.3%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, backgroundColor:'#FF5E2F', borderRadius:30, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Receive")}
                    >
                    <Icon
                        name='ios-car'
                        style={{fontSize:25, color:colors.white}}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>To Receive</Text>
                </View>
              </View>
              <View style={{flex:1, flexDirection:'row', paddingTop:10, paddingBottom:10}}>
                <View style={{width:'33.3%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, backgroundColor:'#F54354', borderRadius:30, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Completed")}
                    >
                    <Icon
                        name='appstore'
                        style={{fontSize:25, color:colors.white}}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Completed</Text>
                </View>
                <View style={{width:'33.3%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, backgroundColor:'#EB9201', borderRadius:30, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Cancelled")}
                    >
                    <Icon
                        name='ios-open'
                        style={{fontSize:25, color:colors.white}}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Cancelled</Text>
                </View>
                <View style={{width:'33.3%', justifyContent: "center", alignItems: "center"}}>
                  <View style={{width:40, height:40, backgroundColor:'#3C9EF6', borderRadius:30, justifyContent: "center", alignItems: "center"}}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Return")}
                    >
                    <Icon
                        name='ios-card'
                        style={{fontSize:25, color:colors.white}}
                    />
                    </TouchableOpacity>
                  </View>
                  <Text style={{fontSize:10}}>Return/Refund</Text>
                </View>
              </View>
            </View>
          </View>
          <TouchableOpacity
            transparent
            onPress={() => this.loadRating()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-star'
                    style={{fontSize:25, color:'#EB9201'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>My Rating</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() => this.loadVoucher()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='logo-usd'
                    style={{fontSize:23, color:'#3C9EF6'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>My Vouchers</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() =>
                this.navigateToScreen(
                  "SettingAccount",
                  this.state.dataUser
                )
              }
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{ flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>

                <Icon
                    name='ios-contact'
                    style={{fontSize:25, color:'#0D83D'}}
                />

              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>Account Setting</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() => this.loadHelp()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{ flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='help-circle'
                    style={{fontSize:25, color:'#FF5E2F'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>Help Centre</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() => this.exitApp()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='log-out'
                    style={{fontSize:25, color:'#319F4C'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>Logout</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
        </Content>
      </ScrollView>

      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleRating}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleRating: false });
            }}
            dialogTitle={<DialogTitle title="My Rating" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5}}>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#DEDFDF'}}
                    />
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleVoucher}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleVoucher: false });
            }}
            dialogTitle={<DialogTitle title="My Vouchers" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5, alignItems:'center'}}>
                    <Text style={{fontSize:30, fontWeight:'bold', textAlign:'center'}}>RP. 0,-</Text>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

        <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleHelp}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog }}
              onTouchOutside={() => {
                this.setState({ visibleHelp: false });
              }}
              dialogTitle={<DialogTitle title="Help Centre" />}
              actions={[
                <DialogButton
                  style={{
                    fontSize: 11,
                    backgroundColor: colors.white,
                    borderColor: colors.blue01
                  }}
                  text="SORT"
                  onPress={() => this.loadData()}
                  // onPress={() => this.setState({ visibleFilter: false })}
                />
              ]}
            >
              <DialogContent>
                {
                  <View style={{flexDirection:'row', flex:1, paddingTop:5, alignItems:'center'}}>
                      <Text style={{fontSize:20, fontWeight:'bold', textAlign:'center'}}>official@kayuku.id</Text>
                  </View>
                }
              </DialogContent>
            </Dialog>
          </View>
      <CustomFooter navigation={this.props.navigation} menu="Account" />
      </Container>
      );
  }
}
