import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Receive extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
      AsyncStorage.getItem("profil").then(dataUser => {
          this.setState({
            customers_id: JSON.parse(dataUser).id,
          });
          this.loadOrder()
      });
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadOrder();
      });
    }

    loadOrder(){
      this.setState({ isLoading: true });
      var url = GlobalConfig.SERVERHOST + 'list_order';

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'GET',
      }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.status == 200) {
                this.setState({
                  listTroli: responseJson.data.filter(x => x.customers_id == this.state.customers_id && x.status == 'shipping'),
                  isLoading: false
                });
            }
        })
    }

    KonfirmasiCompleted(index) {
      Alert.alert(
        'Information',
        'Do you want to COMPLETED this product?',
        [
          { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'YES', onPress: () => this.completed(index) },
        ],
        { cancelable: false }
      );
    }

    completed(index){
      var url = GlobalConfig.SERVERHOST + 'updateOrder';
      var formData = new FormData();
      formData.append("id", this.state.listTroli[index].id)
      formData.append("customers_id", this.state.listTroli[index].customers_id)
      formData.append("customers_name", this.state.listTroli[index].customers_name)
      formData.append("toko_id", this.state.listTroli[index].toko_id)
      formData.append("kode", this.state.listTroli[index].kode)
      formData.append("name", this.state.listTroli[index].name)
      formData.append("image", this.state.listTroli[index].image)
      formData.append("status_image", this.state.listTroli[index].status_image)
      formData.append("harga", this.state.listTroli[index].harga)
      formData.append("status", 'completed')
      formData.append("qty", this.state.listTroli[index].qty)
      formData.append("total", this.state.listTroli[index].total)
      formData.append("alamat", this.state.listTroli[index].alamat)
      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.status == 200) {
            Alert.alert('Succes', 'Completed Product Success', [{
              text: 'OK'
            }]);
            this.loadOrder()
          } else{
            Alert.alert('Error', 'Completed Product Failed', [{
              text: 'OK'
            }]);
          }
      });
    }

    render() {
      var listOrder;
      if (this.state.isLoading) {
        listOrder = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <ActivityIndicator size="large" color="#330066" animating />
          </View>
        );
      } else {
          if (this.state.listTroli == '') {
            listOrder = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {
            listOrder = this.state.listTroli.map((listOrder, index) => (
              <View key={index}>
              <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
                <View style={{flexDirection:'row', marginBottom:10, marginTop:10}}>
                  <View style={{width:'25%', justifyContent: "center"}}>
                    <View style={{width:75, height:75}}>
                      <Image
                        source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listOrder.image }}
                        style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
                      />
                    </View>
                  </View>
                  <View style={{width:'50%'}}>
                    <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{listOrder.name}</Text>
                    <Text style={{fontSize:12}}>QTY : {listOrder.qty}</Text>
                    <Text style={{fontSize:12}}>Address : {listOrder.alamat}</Text>
                    <Text style={{fontSize:13, paddingTop:5, color:colors.secondary, fontWeight:'bold'}}>Rp. {listOrder.total}</Text>
                  </View>
                  <View style={{width:'25%'}}>
                      <Button
                        block
                        style={{
                          width:'100%',
                          marginTop:10,
                          height: 35,
                          marginBottom: 10,
                          borderWidth: 0,
                          backgroundColor: colors.primarydark,
                          borderRadius: 15
                        }}
                        onPress={() => this.KonfirmasiCompleted(index)}
                      >
                        <Text style={{color:colors.white}}>Finish</Text>
                      </Button>
                  </View>
                </View>
              </View>
              </View>
            ))
          }
        }
        return (
            <Container style={styles.wrapper}>
              <Header style={styles.header}>
                <Left style={{ flex: 1 }}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Account")}
                    >
                      <Icon
                        name='arrow-back'
                        size={10}
                        style={{color:colors.black, fontSize:20}}
                      />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex:3, alignItems:'center' }}>
                  <Title style={{fontSize:15, color:colors.black}}>My Order</Title>
                </Body>
                <Right style={{ flex: 1 }}>

                </Right>
              </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                  <Footer>
                    <FooterTab style={styles.tabfooter}>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToPay")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Pay</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToShip")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Ship</Text>
                    </Button>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:colors.black, fontSize:10}}>To Receive</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.graydark}}></View>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Completed")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Completed</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Cancelled")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Cancelled</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Return")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Return</Text>
                    </Button>
                    </FooterTab>
                </Footer>
                <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
                  {listOrder}
                </View>
            </Container>
        );
    }
}
