import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Completed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
      AsyncStorage.getItem("profil").then(dataUser => {
          this.setState({
            customers_id: JSON.parse(dataUser).id,
          });
          this.loadOrder()
      });
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadOrder();
      });
    }

    loadOrder(){
      this.setState({ isLoading: true });
      var url = GlobalConfig.SERVERHOST + 'list_order';

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'GET',
      }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.status == 200) {
                this.setState({
                  listTroli: responseJson.data.filter(x => x.customers_id == this.state.customers_id && x.status == 'completed'),
                  isLoading: false
                });
            }
        })
    }

    render() {
      var listOrder;
      if (this.state.isLoading) {
        listOrder = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <ActivityIndicator size="large" color="#330066" animating />
          </View>
        );
      } else {
          if (this.state.listTroli == '') {
            listOrder = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {
            listOrder = this.state.listTroli.map((listOrder, index) => (
              <View key={index}>
              <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
                <View style={{flexDirection:'row', marginBottom:10, marginTop:10}}>
                  <View style={{width:'25%', justifyContent: "center"}}>
                    <View style={{width:75, height:75}}>
                      <Image
                        source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listOrder.image }}
                        style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
                      />
                    </View>
                  </View>
                  <View style={{width:'75%'}}>
                    <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{listOrder.name}</Text>
                    <Text style={{fontSize:12}}>QTY : {listOrder.qty}</Text>
                    <Text style={{fontSize:12}}>Address : {listOrder.alamat}</Text>
                    <Text style={{fontSize:13, paddingTop:5, color:colors.secondary, fontWeight:'bold'}}>Rp. {listOrder.total}</Text>
                  </View>
                </View>
              </View>
              </View>
            ))
          }
        }
        return (
            <Container style={styles.wrapper}>
              <Header style={styles.header}>
                <Left style={{ flex: 1 }}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Account")}
                    >
                      <Icon
                        name='arrow-back'
                        size={10}
                        style={{color:colors.black, fontSize:20}}
                      />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex:3, alignItems:'center' }}>
                  <Title style={{fontSize:15, color:colors.black}}>My Order</Title>
                </Body>
                <Right style={{ flex: 1 }}>

                </Right>
              </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                  <Footer>
                    <FooterTab style={styles.tabfooter}>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToPay")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Pay</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToShip")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Ship</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Receive")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Receive</Text>
                    </Button>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:colors.black, fontSize:10}}>Completed</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.graydark}}></View>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Cancelled")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Cancelled</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Return")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Return</Text>
                    </Button>
                    </FooterTab>
                </Footer>
                <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
                  {listOrder}
                </View>
            </Container>
        );
    }
}
