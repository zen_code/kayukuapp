import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Return extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
      AsyncStorage.getItem("profil").then(dataUser => {
          this.setState({
            customers_id: JSON.parse(dataUser).id,
          });
          this.loadOrder()
      });
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadOrder();
      });
    }

    loadOrder(){
      this.setState({ isLoading: true });
      var url = GlobalConfig.SERVERHOST + 'list_order';

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'GET',
      }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.status == 200) {
                this.setState({
                  listTroli: responseJson.data.filter(x => x.customers_id == this.state.customers_id && x.status == 'return'),
                  isLoading: false
                });
            }
        })
    }

    render() {
      var listOrder;
      if (this.state.isLoading) {
        listOrder = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <ActivityIndicator size="large" color="#330066" animating />
          </View>
        );
      } else {
          if (this.state.listTroli == '') {
            listOrder = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {

          }
      }
        return (
            <Container style={styles.wrapper}>
              <Header style={styles.header}>
                <Left style={{ flex: 1 }}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Account")}
                    >
                      <Icon
                        name='arrow-back'
                        size={10}
                        style={{color:colors.black, fontSize:20}}
                      />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex:3, alignItems:'center' }}>
                  <Title style={{fontSize:15, color:colors.black}}>My Order</Title>
                </Body>
                <Right style={{ flex: 1 }}>

                </Right>
              </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                  <Footer>
                    <FooterTab style={styles.tabfooter}>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToPay")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Pay</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToShip")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Ship</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Receive")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Receive</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Completed")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Completed</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Cancelled")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Cancelled</Text>
                    </Button>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:colors.black, fontSize:10}}>Return</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.graydark}}></View>
                    </Button>
                    </FooterTab>
                </Footer>
                <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
                  {listOrder}
                </View>
            </Container>
        );
    }
}
