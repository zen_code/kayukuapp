import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import ImagePicker from "react-native-image-picker";

export default class Topay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
            customers_id:'',
            listTroli:[],
            visibleDetail:false,
            pickedImage:'',
            uri:'',
            fileType:'',

        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
      AsyncStorage.getItem("profil").then(dataUser => {
          this.setState({
            customers_id: JSON.parse(dataUser).id,
          });
          this.loadTroli()
      });
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadTroli();
      });
    }

    loadTroli(){
      this.setState({ isLoading: true });
      var url = GlobalConfig.SERVERHOST + 'list_order';

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'GET',
      }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.status == 200) {
                this.setState({
                  listTroli: responseJson.data.filter(x => x.customers_id == this.state.customers_id && x.status == 'order'),
                  isLoading: false,
                  visibleDetail: false,
                  pickedImage:'',
                  uri:'',
                  fileType:''
                });
            }
        })
    }

    KonfirmasiCancel(index) {
      Alert.alert(
        'Information',
        'Do you want to CANCEL this product?',
        [
          { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'YES', onPress: () => this.Cancel(index) },
        ],
        { cancelable: false }
      );
    }

    Cancel(index){
      var url = GlobalConfig.SERVERHOST + 'updateOrder';
      var formData = new FormData();
      formData.append("id", this.state.listTroli[index].id)
      formData.append("customers_id", this.state.listTroli[index].customers_id)
      formData.append("customers_name", this.state.listTroli[index].customers_name)
      formData.append("toko_id", this.state.listTroli[index].toko_id)
      formData.append("kode", this.state.listTroli[index].kode)
      formData.append("name", this.state.listTroli[index].name)
      formData.append("image", this.state.listTroli[index].image)
      formData.append("status_image", this.state.listTroli[index].status_image)
      formData.append("harga", this.state.listTroli[index].harga)
      formData.append("status", 'cancel')
      formData.append("qty", this.state.listTroli[index].qty)
      formData.append("total", this.state.listTroli[index].total)
      formData.append("alamat", this.state.listTroli[index].alamat)
      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.status == 200) {
            Alert.alert('Succes', 'Cancel Product Success', [{
              text: 'OK'
            }]);
            this.loadTroli()
          } else{
            Alert.alert('Error', 'Cancel Product Failed', [{
              text: 'OK'
            }]);
          }
      });
    }

    sendTransfer(index){
      if(this.state.uri==''){
        Alert.alert(
          'Information',
          'Upload Struk Transfer',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
      } else {
        this.sendTransferKonfirmasi(index)
      }

    }

    sendTransferKonfirmasi(index) {
      var url = GlobalConfig.SERVERHOST + 'updateOrder';
      var formData = new FormData();
      formData.append("id", this.state.listTroli[index].id)
      formData.append("customers_id", this.state.listTroli[index].customers_id)
      formData.append("customers_name", this.state.listTroli[index].customers_name)
      formData.append("toko_id", this.state.listTroli[index].toko_id)
      formData.append("kode", this.state.listTroli[index].kode)
      formData.append("name", this.state.listTroli[index].name)
      formData.append("image", this.state.listTroli[index].image)
      if (this.state.uri!='') {
        formData.append("status_image", {
          uri: this.state.uri,
          type: this.state.fileType,
          name: 'imageKonfirmasi_'+ this.state.listTroli[index].name
      });
      }
      formData.append("harga", this.state.listTroli[index].harga)
      formData.append("status", 'transferSucces')
      formData.append("qty", this.state.listTroli[index].qty)
      formData.append("total", this.state.listTroli[index].total)
      formData.append("alamat", this.state.listTroli[index].alamat)
      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.status == 200) {
            Alert.alert('Succes', 'Confirmation Success', [{
              text: 'OK'
            }]);
            this.setState({
              visibleDetail: false,
              pickedImage:'',
              uri:'',
              fileType:''
            });
            this.loadTroli()
          } else{
            Alert.alert('Error', 'Confirmation Failed', [{
              text: 'OK'
            }]);
          }
      });
    }

    loadDetail(){
      this.setState({
        visibleDetail: true,
        pickedImage:'',
        uri:'',
        fileType:''
      });
    }

    pickImageHandlerUpload = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImage: res.uri,
                  uri: res.uri,
                  fileType:res.type
              });

          }
      });
    }

    render() {
      var listOrder;
      if (this.state.isLoading) {
        listOrder = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <ActivityIndicator size="large" color="#330066" animating />
          </View>
        );
      } else {
          if (this.state.listTroli == '') {
            listOrder = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {
        listOrder = this.state.listTroli.map((listOrder, index) => (
          <View key={index}>
          <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
            <View style={{flexDirection:'row', marginBottom:10, marginTop:10}}>
              <View style={{width:'25%', justifyContent: "center"}}>
                <View style={{width:75, height:75}}>
                  <Image
                    source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listOrder.image }}
                    style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
                  />
                </View>
              </View>
              <View style={{width:'65%'}}>
                <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{listOrder.name}</Text>
                <Text style={{fontSize:12}}>QTY : {listOrder.qty}</Text>
                <Text style={{fontSize:12}}>Address : {listOrder.alamat}</Text>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{width:'65%'}}>
                    <Text style={{fontSize:13, paddingTop:5, color:colors.secondary, fontWeight:'bold'}}>Rp. {listOrder.total}</Text>
                  </View>
                  <View style={{width:'35%', flex:1, flexDirection:'row'}}>
                  <TouchableOpacity
                    transparent
                    onPress={() => this.loadDetail()}>
                    <Text style={{fontSize:12, paddingTop:5, paddingRight:5}}>Detail Order</Text>
                  </TouchableOpacity>
                    <Icon
                      name='arrow-dropright'
                      style={{fontSize:15, paddingTop:5}}
                    />
                  </View>
                </View>
              </View>
              <View style={{width:'10%', alignItems:'center', justifyContent:'center'}}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.KonfirmasiCancel(index)}>
                  <Icon
                    name='ios-trash'
                    style={{fontSize:15, color:colors.red}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ width: '100%', position: "absolute"}}>
              <Dialog
                visible={this.state.visibleDetail}
                dialogAnimation={
                  new SlideAnimation({
                    slideFrom: "bottom"
                  })
                }
                dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
                onTouchOutside={() => {
                  this.setState({ visibleDetail: false });
                }}
              >
                <DialogContent>
                  {
                  <View>
                    <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                      <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Payment</Text>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{}}>
                        <Text style={{fontSize:12}}>Virtual Account Number :</Text>
                        <Text style={{fontSize:17, color:colors.secondary}}>{listOrder.kode}</Text>
                        <Text style={{fontSize:12}}>Accepts transfer from all banks</Text>
                      </View>
                    </View>
                    <View style={{marginTop:10, width:'100%', height:1, backgroundColor:colors.graydark}}>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{width:'10%'}}>
                        <View style={{backgroundColor:colors.graydark, borderRadius:50, width:15, height:15, alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:colors.white, fontSize:10}}>1</Text>
                        </View>
                      </View>
                      <View style={{width:'90%'}}>
                        <Text style={{fontSize:11}}>Choose Payment/Purchase</Text>
                      </View>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{width:'10%'}}>
                        <View style={{backgroundColor:colors.graydark, borderRadius:50, width:15, height:15, alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:colors.white, fontSize:10}}>2</Text>
                        </View>
                      </View>
                      <View style={{width:'90%'}}>
                        <Text style={{fontSize:11}}>Choose Others > Others > Multi Payment</Text>
                      </View>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{width:'10%'}}>
                        <View style={{backgroundColor:colors.graydark, borderRadius:50, width:15, height:15, alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:colors.white, fontSize:10}}>3</Text>
                        </View>
                      </View>
                      <View style={{width:'90%'}}>
                        <Text style={{fontSize:11}}>Input Company code 73140 and choose Correct</Text>
                      </View>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{width:'10%'}}>
                        <View style={{backgroundColor:colors.graydark, borderRadius:50, width:15, height:15, alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:colors.white, fontSize:10}}>4</Text>
                        </View>
                      </View>
                      <View style={{width:'90%'}}>
                        <Text style={{fontSize:11}}>Input payment code {listOrder.kode} and choose Correct</Text>
                      </View>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{width:'10%'}}>
                        <View style={{backgroundColor:colors.graydark, borderRadius:50, width:15, height:15, alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:colors.white, fontSize:10}}>5</Text>
                        </View>
                      </View>
                      <View style={{width:'90%'}}>
                        <Text style={{fontSize:11}}>Check the data on screen and choose Yes</Text>
                      </View>
                    </View>
                    <View style={{marginTop:10, width:'100%', height:1, backgroundColor:colors.graydark}}>
                    </View>
                    <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                      <View style={{width:'70%'}}>
                        <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{listOrder.name}</Text>
                        <Text style={{fontSize:12, fontWeight:'bold'}}>QTY : {listOrder.qty}</Text>
                        <Text style={{fontSize:12, fontWeight:'bold'}}>Address : {listOrder.alamat}</Text>
                        <Text style={{fontSize:13, paddingTop:5, color:colors.secondary, fontWeight:'bold'}}>Rp. {listOrder.total}</Text>
                      </View>
                      <View style={{width:'30%'}}>
                        <View style={{alignItems:'center'}}>
                          <TouchableOpacity
                            transparent
                            onPress={this.pickImageHandlerUpload}
                          >
                            <View style={styles.placeholder}>
                                <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <View>
                        <Button
                          block
                          style={{
                            width:'100%',
                            marginTop:10,
                            height: 35,
                            marginBottom: 5,
                            borderWidth: 0,
                            backgroundColor: colors.primarydark,
                            borderRadius: 15
                          }}
                          onPress={() => this.sendTransfer(index)}
                        >
                          <Text style={{color:colors.white}}>Send</Text>
                        </Button>
                        </View>
                      </View>
                    </View>
                  </View>
                  }
                </DialogContent>
              </Dialog>
            </View>
          </View>
        ))
      }
      }
        return (
            <Container style={styles.wrapper}>
              <Header style={styles.header}>
                <Left style={{ flex: 1 }}>
                    <TouchableOpacity
                      transparent
                      onPress={() => this.props.navigation.navigate("Account")}
                    >
                      <Icon
                        name='arrow-back'
                        size={10}
                        style={{color:colors.black, fontSize:20}}
                      />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex:3, alignItems:'center' }}>
                  <Title style={{fontSize:15, color:colors.black}}>My Order</Title>
                </Body>
                <Right style={{ flex: 1 }}>

                </Right>
              </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                  <Footer>
                    <FooterTab style={styles.tabfooter}>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:colors.black, fontSize:10}}>To Pay</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.graydark}}></View>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("ToShip")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Ship</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Receive")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>To Receive</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Completed")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Completed</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Cancelled")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Cancelled</Text>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("Return")
                        }>
                        <Text style={{color:colors.black, fontSize:10}}>Return</Text>
                    </Button>
                    </FooterTab>
                </Footer>
                <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
                  {listOrder}
                </View>
            </Container>
        );
    }
}
