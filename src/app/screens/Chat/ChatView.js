import React from 'react';
import {
   View,
   ListView,
   Image,
   Text,
   TouchableOpacity,
   StyleSheet,
   TextInput,
   Dimensions,
   KeyboardAvoidingView,
 } from 'react-native';
import {
  Footer,
  FooterTab,
  Icon
} from 'native-base'
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import Icons from 'react-native-vector-icons/MaterialIcons';
import colors from "../../../styles/colors";

const { width, height } = Dimensions.get('window');
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const conversation = [
  {
    sent: true,
    msg: 'Hallo',
  },
  {
    sent: false,
    msg: 'Ada yang bisa kami bantu',
  },
];



const EachMsg = (props) => {
  if (props.sent === false) {
    return (
      <View style={styles.eachMsg}>
        {/*<Image source={{ uri: props.image }}style={styles.userPic} />*/}
        <Image source={require('../../../assets/images/profil2.jpg')} style={styles.userPic} />
        <View style={styles.msgBlock}>
          <Text style={styles.msgTxt}>{props.msg}</Text>
        </View>
      </View>
    );
  }
  return (
    <View style={styles.rightMsg} >
      <View style={styles.rightBlock} >
        <Text style={styles.rightTxt}>{props.msg}</Text>
      </View>
      <Image source={require('../../../assets/images/profil1.jpg')} style={styles.userPic} />
    </View>
  );
};

class Chatview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(conversation),
      msg: '',
    };
    this.send = this.send.bind(this);
    this.reply = this.reply.bind(this);
  }

  static navigationOptions = {
    header: null
  };

  reply() {
    conversation.unshift({
      sent: false,
      msg: 'Ada yang bisa kami bantu!',
    });
    this.setState({
      dataSource: ds.cloneWithRows(conversation),
    });
  }

  send() {
    if (this.state.msg.length > 0) {
      conversation.unshift({
        sent: true,
        msg: this.state.msg,
      });
      this.setState({
        dataSource: ds.cloneWithRows(conversation),
        msg: '',
      });
      setTimeout(() => {
        this.reply();
      }, 2000);
    }
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor:colors.gray}}>

          <View style={styles.header}>
            <View style={styles.left} >
              <TouchableOpacity onPress={() => this.props.navigation.navigate("ChatStore")}>
                <Icons
                  name="arrow-back" color="#fff" size={23}
                  style={{ paddingLeft: 10 }}
                />
              </TouchableOpacity>
              <Image
                source={require("../../../assets/images/profil1.jpg")}
                style={styles.chatImage}
              />
              <TouchableOpacity
                onPress={() => {
                  this.props.navigator.push({
                    id: 'ProfileView',
                    name: this.props.name,
                    image: this.props.image,
                  });
                }}
              >
                <Text style={styles.chatTitle}>{this.props.name}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.right} >
              <Icons name="attach-file" color="#fff" size={23} style={{ padding: 5 }} />
              <Icons name="more-vert" color="#fff" size={23} style={{ padding: 5 }} />
            </View>
          </View>
            <ListView
              enableEmptySections
              noScroll
              renderScrollComponent={props =>
                <InvertibleScrollView {...props} inverted />}
              dataSource={this.state.dataSource}
              contentContainerStyle={{ justifyContent: 'flex-end' }}
              renderRow={rowData => <EachMsg {...rowData} image={require("../../../assets/images/profil1.jpg")} />}
              style={{ flex: 1 }}
            />
          <Footer>
            <FooterTab style={{ backgroundColor: colors.gray}}>
            <View style={{flex:1, flexDirection:'row'}}>
            <View style={styles.input}>
              <TextInput
                style={{ flex: 1, color:colors.black, paddingLeft:5, paddingRight:5}}
                value={this.state.msg}
                onChangeText={msg => this.setState({ msg })}
                blurOnSubmit={false}
                onSubmitEditing={() => this.send()}
                placeholder="Type a message"
                returnKeyType="send"
              />
            </View>
            <View style={styles.inputBtn}>
              <TouchableOpacity
                transparent
                onPress={() => this.send()}
              >
              <Icon
                name='send'
                style={{fontSize:30, paddingLeft:10, color:colors.black}}
              />
              </TouchableOpacity>
            </View>
            </View>
            </FooterTab>
          </Footer>
      </View>);
  }
        }

export default Chatview;

const styles = StyleSheet.create({
  keyboard: {
    flex: 1,
    justifyContent: 'center',
  },
  image: {
    width,
    height,
  },
  header: {
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.secondary,
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  right: {
    flexDirection: 'row',
  },
  chatTitle: {
    color: '#fff',
    fontWeight: '600',
    margin: 10,
    fontSize: 15,
  },
  chatImage: {
    width: 30,
    height: 30,
    borderRadius: 15,
    margin: 5,
  },
  input: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    padding: 0,
    height: 40,
    width: '80%',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.graydark,
    backgroundColor: colors.white,
    marginLeft: 10,
    marginBottom: 10,
    marginRight: 3,
    shadowColor: '#3d3d3d',
    shadowRadius: 2,
    shadowOpacity: 0.5,
    shadowOffset: {
    height: 1,
    },
  },
  inputBtn: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    padding: 5,
    height: 40,
    borderRadius:10,
    backgroundColor: colors.graydark,
    marginBottom: 10,
    marginRight: 10,
    shadowColor: '#3d3d3d',
    shadowRadius: 2,
    shadowOpacity: 0.5,
    shadowOffset: {
    height: 1,
    },
  },
  eachMsg: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    margin: 5,
  },
  rightMsg: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    margin: 5,
    alignSelf: 'flex-end',
  },
  userPic: {
    height: 40,
    width: 40,
    margin: 5,
    borderRadius: 20,
    backgroundColor: '#f8f8f8',
  },
  msgBlock: {
    paddingLeft:10,
    paddingRight:10,
    borderRadius: 5,
    backgroundColor: colors.white,
    padding: 10,
    shadowColor: '#3d3d3d',
    shadowRadius: 2,
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 1,
    },
  },
  rightBlock: {
    paddingLeft:10,
    paddingRight:10,
    borderRadius: 5,
    backgroundColor: colors.graydark,
    padding: 10,
    shadowColor: '#3d3d3d',
    shadowRadius: 2,
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 1,
    },
  },
  msgTxt: {
    fontSize: 15,
    color: '#555',
  },
  rightTxt: {
    fontSize: 15,
    color: '#202020',
  },
});
