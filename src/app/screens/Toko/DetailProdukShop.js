import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/DetailProduk";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class DetailProdukShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      listProduk: [],
      id:'',
      name:'',
      deskripsi:'',
      harga:'',
      image_primary:'',
      visibleShare:false
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
      AsyncStorage.getItem('listProduk').then((listProduk) => {
        this.setState({
          listProduk:listProduk,
          id: JSON.parse(listProduk).id,
          name: JSON.parse(listProduk).name,
          deskripsi: JSON.parse(listProduk).deskripsi,
          harga: JSON.parse(listProduk).harga,
          image_primary: JSON.parse(listProduk).image_primary,
        });

      })
  }

  loadShare(){
    this.setState({
      visibleShare: true
    });
  }

  deleteProduct(){
    Alert.alert(
      'Confirmation',
      'Do you want to delete this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Delete', onPress: () => this.konfirmasiDelete() },
      ],
      { cancelable: false }
    );
  }

  konfirmasiDelete(){
    var url = GlobalConfig.SERVERHOST + 'delete_product';
    var formData = new FormData();
    formData.append("id", this.state.id)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200) {
          Alert.alert('Success', 'Delete Product Success', [{
            text: 'OK'
          }]);
          this.props.navigation.navigate("MyProduct");
        } else{
          Alert.alert('Error', 'Delete Product Failed', [{
            text: 'OK'
          }]);
        }
    });
    }

  navigateToScreen(route, listToko) {
    AsyncStorage.setItem("listToko", listToko).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <Content style={{ marginTop:0}}>
          <View style={{width: '100%', height:300}}>
            <Image
              style={{
                marginRight:10,
                alignSelf: "center",
                width: '100%',
                height: '100%',
                borderBottomRightRadius:50
              }}
              source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.state.image_primary }}/>
            <TouchableOpacity
              transparent
              style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
              onPress={()=>this.props.navigation.navigate("MyProduct")}
            >
              <Icon
                name='arrow-back'
                size={10}
                style={{color:colors.white, fontSize:20}}
              />
            </TouchableOpacity>
          </View>
          <View style={{width: '100%', height:50, marginTop:-30, flex:1, flexDirection:'row'}}>
            <View style={{width:'60%'}}>
            </View>
            <View style={{width:'40%', height:50, justifyContent: "center", alignItems: "center"}}>
              <View style={{width:50, height:50, borderRadius:40, backgroundColor:colors.white, justifyContent: "center", alignItems: "center"}}>
                <Icon
                  name='ios-heart'
                  size={10}
                  style={{fontSize:30, color:colors.red}}
                />
              </View>
            </View>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:-5,}}>
            <Text style={{fontSize:15, color:colors.black, fontWeight:'bold'}}>{this.state.name}</Text>
            <Text style={{fontSize:15, paddingTop:5, color:colors.primarydark, fontWeight:'bold'}}>Rp.{this.state.harga}</Text>
            <Text style={{fontSize:12, paddingTop:0,}}>{this.state.deskripsi}</Text>
          </View>
          <View style={{flex:1, flexDirection:'row', paddingLeft:10, paddingRight:10, marginTop:10}}>
              <View style={{width:'40%', flex:1, flexDirection:'row', paddingTop:10, paddingLeft:10, paddingBottom:10, borderWidth:1, borderRadius:5, borderColor:colors.graydark, alignItems: 'center', justifyContent:'center'}}>
                <Icon
                  name='ios-cart'
                  style={{fontSize:15, paddingRight:10}}
                />
                <Text style={{fontSize:10, paddingRight:10}}>Sold</Text>
                <Text style={{fontSize:10}}>0</Text>
              </View>
              <View style={{width:'40%', flex:1, flexDirection:'row', paddingTop:10, paddingLeft:10, paddingBottom:10, borderWidth:1, borderRadius:5, borderColor:colors.graydark, alignItems: 'center', justifyContent:'center', marginRight:5, marginLeft:5}}>
                <Icon
                  name='ios-star-outline'
                  style={{fontSize:15, paddingRight:10}}
                />
                <Text style={{fontSize:10, paddingRight:5}}>Rating</Text>
                <Text style={{fontSize:10}}>0</Text>
              </View>

              <View style={{width:'20%', flex:1, flexDirection:'row', paddingTop:10, paddingLeft:10, paddingBottom:10, borderWidth:1, borderRadius:5, borderColor:colors.graydark, alignItems: 'center', justifyContent:'center'}}>
                <TouchableOpacity
                  transparent
                  onPress={()=>this.loadShare()}
                >
                <Icon
                  name='share'
                  style={{fontSize:15, paddingRight:10}}
                />
                </TouchableOpacity>
              </View>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, borderWidth:1, borderRadius:30, borderColor:colors.secondary, alignItems: 'center', justifyContent:'center', marginRight:5}}>
                <Icon
                  name='pricetags'
                  style={{fontSize:15, color:colors.secondary, paddingRight:10}}
                />
                <TouchableOpacity
                  transparent
                  onPress={() => this.navigateToScreen("UpdateProduct", this.state.listProduk)}
                >
                <Text style={{fontSize:12, fontWeight:'bold', color:colors.secondary}}>Update Product</Text>
                </TouchableOpacity>
              </View>

              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, borderWidth:1, borderRadius:30, borderColor:colors.secondary, alignItems: 'center', justifyContent:'center', marginLeft:5}}>
                <Icon
                  name='trash'
                  style={{fontSize:15, color:colors.secondary, paddingRight:10}}
                />
                <TouchableOpacity
                  transparent
                  onPress={()=>this.deleteProduct()}
                >
                <Text style={{fontSize:12, fontWeight:'bold', color:colors.secondary}}>Delete Product</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Content>
      </ScrollView>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleShare}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleShare: false });
            }}
            dialogTitle={<DialogTitle title="Share Product" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5}}>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-facebook'
                        style={{fontSize:35, color:'#3850A0'}}
                    />
                  </View>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-instagram'
                        style={{fontSize:35, color:'#E55098'}}
                    />
                  </View>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-whatsapp'
                        style={{fontSize:35, color:'#29B573'}}
                    />
                  </View>
                  <View style={{paddingRight:0}}>
                    <Icon
                        name='logo-twitter'
                        style={{fontSize:35, color:'#2585C6'}}
                    />
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
      </Container>
      );
  }
}
