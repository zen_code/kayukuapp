import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Thumbnail
} from "native-base";
import styles from "./../styles/AllProduk";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProdukShop from "../../components/SubProdukShop";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listProduk) {
    AsyncStorage.setItem("listProduk", JSON.stringify(listProduk)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'50%', paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailProdukShop", this.props.data)}>
        <SubProdukShop
          imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image_primary }}
          name={this.props.data.name}
          harga={this.props.data.harga}
          terjual={this.props.data.stok}
          favorite={this.props.data.berat}
        />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class MyProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      idToko:'',
      name:'',
      listProduk:[],
      listShop:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
      AsyncStorage.getItem('listShop').then((listShop) => {
        this.setState({
          listShop:listShop,
          idToko: JSON.parse(listShop).id,
          name: JSON.parse(listShop).name
        });
        this.loadData();
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
  }

  loadData(){
    var url = GlobalConfig.SERVERHOST + 'get_product_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listProduk: responseJson.data.filter(x => x.toko_id == this.state.idToko),
                  isLoading: false
              });
          }
      })
  }

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  navigateToScreen(route, listShop) {
    AsyncStorage.setItem("listShop", listShop).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that = this;
    var listProduk;
    if (this.state.isLoading) {
      listProduk = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        if (this.state.listProduk == '') {
          listProduk = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Data!</Text>
            </View>
          );
        } else {
          listProduk = (
            <FlatList
              data={this.state.listProduk}
              renderItem={this._renderItem}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />
          );
        }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={()=>this.props.navigation.navigate('MyShop')}
            >
              <Icon
                name='arrow-back'
                size={10}
                style={{color:colors.black, fontSize:20}}
              />
            </TouchableOpacity>
        </Left>
        <Body style={{ flex:3, alignItems:'center' }}>
          <Title style={{fontSize:15, color:colors.black}}>{this.state.name}</Title>
        </Body>
        <Right style={{ flex: 1 }}>
          <TouchableOpacity
            transparent
            onPress={() => this.navigateToScreen("AddProduct", this.state.listShop)}
          >
          <Icon
            name='add'
            style={{paddingRight:4, fontSize:20, color:colors.secondary}}
          />
          </TouchableOpacity>
        </Right>
      </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
        {listProduk}
      </View>
      <CustomFooter navigation={this.props.navigation} menu="Favorite" />
      </Container>
      );
  }
}
