import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import CustomRadioButton from "../components/CustomRadioButton"
import GlobalConfig from "../components/GlobalConfig";

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      isMale: true,
      isFemale: false,
      name:'',
      email:'',
      jenis_kelamin:'Laki-Laki',
      username:'',
      password:'',
      passwordValidasi:'',
      dataUser:'',
      dataUserId:'',

    };
  }

  static navigationOptions = {
    header: null
  };

  konfirmasiRegister(){
    if(this.state.isMale == true){
      this.setState({jenis_kelamin:'Laki-Laki'})
    } else {
      this.setState({jenis_kelamin:'Perempuan'})
    }
    if(this.state.name==""){
      Alert.alert(
        'Information',
        'Input Full Name',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.email==""){
      Alert.alert(
        'Information',
        'Input Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.username==""){
      Alert.alert(
        'Information',
        'Input Username',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password==""){
      Alert.alert(
        'Information',
        'Input Password',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.passwordValidasi==""){
      Alert.alert(
        'Information',
        'Input Password Validasi',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {

      if(this.state.password != this.state.passwordValidasi)
      {
        Alert.alert(
          'Information',
          'Input Password Validasi False',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
      } else {
          var url = GlobalConfig.SERVERHOST + 'register';
          var formData = new FormData();
          formData.append("name", this.state.name)
          formData.append("email", this.state.email)
          formData.append("jenis_kelamin", this.state.jenis_kelamin)
          formData.append("username", this.state.username)
          formData.append("password", this.state.password)

          fetch(url, {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
          }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.status == 200) {
                this.setState({
                  dataUser:responseJson.data,
                  dataUserId:responseJson.data.id,
                })
                this.registerToko();
              } else{
                Alert.alert('Error', 'Register Failed', [{
                  text: 'OK'
                }]);
              }
          });
      }
    }
  }

  registerToko(){
    var url = GlobalConfig.SERVERHOST + 'register_toko';
    var formData = new FormData();
    formData.append("user_id", this.state.dataUserId)
    formData.append("name", this.state.username)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200){
          AsyncStorage.setItem('profil', JSON.stringify(this.state.dataUser)).then(() => {
            Alert.alert('Success', 'Register Success', [{
              text: 'OK'
            }])
            this.props.navigation.navigate("HomeMenu");
          })
        } else {
          Alert.alert('Error', 'Register Failed', [{
            text: 'OK'
          }])
        }

      })
  }



  logOut(){
    this.props.navigation.navigate("SplashScreen")
  }

  render() {
    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff", width:'100%' }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: '100%',
                  height: 300,
                }}
                source={require("../../assets/images/head-img.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.logOut()}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 20, marginTop:-250, paddingBottom:40 }}>
              <View>
                <Text style={styles.signIn}>REGISTER</Text>
              </View>
              <Form style={{ marginLeft: 0, marginRight:20, marginTop:5}}>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Full Name</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='contact'
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.name} onChangeText={(text) => this.setState({ name: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Email</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Gender</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{marginLeft:1, width:'25%'}}>
                      <CustomRadioButton
                          name='Male'
                          selected={this.state.isMale}
                          onPress={() => this.setState({isMale:true, isFemale:false})}/>
                      </View>
                      <View style={{marginLeft:1, width:'75%'}}>
                      <CustomRadioButton
                          name='Female'
                          selected={this.state.isFemale}
                          onPress={() => this.setState({isFemale:true, isMale:false})}/>
                      </View>
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Username Kayuku</Label>
                  <View style={{flex:1, flexDirection:'row', paddingTop:5}}>
                    <View style={{width:'35%', alignItems: 'center', justifyContent: 'center', backgroundColor:colors.primary}}>
                        <Text style={{fontSize:12}}>Kayuku.com/</Text>
                    </View>
                    <View style={{width:'65%', borderWidth:1, borderColor:colors.primary}}>
                        <TextInput style={{height:40}} value={this.state.username} onChangeText={(text) => this.setState({ username: text })} />
                    </View>
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Password</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='unlock'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                    <Input secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Password Validation</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='unlock'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                    <Input secureTextEntry={true} value={this.state.passwordValidasi} onChangeText={(text) => this.setState({ passwordValidasi: text })} />
                  </View>
                </Item>
              </Form>
              </Card>
              <CardItem style={{ borderRadius: 0, marginTop:10}}>
                <View style={{ flex: 1, flexDirection:'column'}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 10,
                      borderWidth: 0,
                      backgroundColor: colors.primarydark,
                      borderRadius: 15
                    }}
                    onPress={() => this.konfirmasiRegister()}
                  >
                    <Text style={{color:colors.white}}>REGISTER</Text>
                  </Button>
                </View>
                <View style={{flex:1, flexDirection:'row', alignItems: 'center', justifyContent: 'center',}}>
                  <View>
                    <Text style={styles.weatherTextLink}>or sign up using</Text>
                  </View>
                  <View style={styles.footerLogo}>
                    <Icon
                      name='logo-facebook'
                      size={10}
                      style={{color:colors.white, marginLeft:12, fontSize:25, marginTop:0}}
                    />
                  </View>
                  <View style={styles.footerLogo}>
                    <Icon
                      name='logo-googleplus'
                      size={10}
                      style={{color:colors.white, marginLeft:10, fontSize:25, marginTop:0}}
                    />
                  </View>
                </View>
                </View>
              </CardItem>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}
