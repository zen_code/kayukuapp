import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    backgroundColor:colors.white
  },
  header: {
    backgroundColor: colors.white
  },
  btnOrder: {
    paddingTop:10,
    paddingBottom:10,
    borderRadius:30,
    width:'100%',
    marginRight:10,
    backgroundColor:colors.secondary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textOrder: {
    fontSize:15,
    fontWeight:'bold',
    color:colors.white
  }


});

export default styles;
