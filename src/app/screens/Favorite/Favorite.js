import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Thumbnail
} from "native-base";
import styles from "./../styles/AllProduk";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProdukFavorite from "../../components/SubProdukFavorite";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listProduk) {
    AsyncStorage.setItem("listProduk", JSON.stringify(listProduk)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  deleteKonfirmasi(index) {
    Alert.alert(
      'Information',
      'Do you want to DELETE this product from FAVORITE?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'DELETE', onPress: () => this.delete(index) },
      ],
      { cancelable: false }
    );
  }

  delete(index){
    var url = GlobalConfig.SERVERHOST + 'deleteFavorite';
    var formData = new FormData();
    formData.append("id", index)
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200) {
          Alert.alert('Succes', 'Delete Product Success', [{
            text: 'OK'
          }]);
          that.loadFavorite();
        } else{
          Alert.alert('Error', 'Delete Product Failed', [{
            text: 'OK'
          }]);
        }
    });
  }

  render() {
    return (
      <View style={{width:'50%', paddingRight:5}}>
      <View style={styles.container}>
        <View style={{width:'100%', height:180, alignItems: 'center', justifyContent: 'center',}}>
          <Image
            source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image_primary }}
            style={{ width: '100%', height:'100%', marginTop: 0, marginBottom: 2, borderTopRightRadius:10, borderTopLeftRadius:10, }}
          />
        </View>
        <View style={{ flex: 1, width:'100%', paddingTop: 3, paddingLeft:10, paddingRight:10}}>
            <Text style={{fontSize:12, color:colors.black, fontWeight: "bold", }}>{this.props.data.name}</Text>
            <Text style={{fontSize:12, paddingTop:5, fontWeight:'bold', color:colors.secondary}}>Rp. {this.props.data.harga}</Text>
        </View>
        <View style={{ flex: 1, width:'100%', flexDirection: 'row', marginTop:25, paddingLeft:10, paddingRight:10, borderBottomRightRadius:10, borderBottomLeftRadius:10,}}>
            <View  style={{width:'50%',flexDirection:'row'}}>
            <TouchableOpacity
              transparent
              onPress={() => this.deleteKonfirmasi(this.props.data.id)}>
              <Icon
                name='ios-heart'
                size={10}
                style={{fontSize:15, color:colors.red}}
              />
             </TouchableOpacity>
              <Text style={{fontSize:10, paddingLeft:4}}>1</Text>
            </View>
            <View style={{width:'50%'}}>
              <Text style={{fontSize:10, textAlign:'right'}}>0 Sold</Text>
            </View>
        </View>
        </View>
      </View>
    );
  }
}

export default class Favorite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      listFavorite:[],
      customers_id:''
    };
  }

  static navigationOptions = {
    header: null
  };
  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadFavorite();
    });
  }
  componentDidMount() {
    AsyncStorage.getItem("profil").then(dataUser => {
        this.setState({
          customers_id: JSON.parse(dataUser).id,
        });
        this.loadFavorite();
    });
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadFavorite();
    });
  }

  loadFavorite(){
    var url = GlobalConfig.SERVERHOST + 'getFavorite';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                listFavorite: responseJson.data.filter(x => x.customers_id == this.state.customers_id),
                isLoading:false
              });
          }
      })
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that=this;
    var listFavorite;
    if (this.state.isLoading) {
      listFavorite = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        if (this.state.listFavorite == '') {
          listFavorite = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Data!</Text>
            </View>
          );
        } else {
          listFavorite = (
            <FlatList
              data={this.state.listFavorite}
              renderItem={this._renderItem}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />
          );
        }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 3 }}>
          <View style={styles.searchHeader}>
              <TextInput style={{height:40, marginLeft:10, marginRight:10}} value={this.state.search} onChangeText={(text) => this.setState({ search: text })} />
          </View>
        </Left>
        <Right style={{ flex: 2, marginLeft:5, justifyContent: "center", alignItems: "center" }}>
          <Text style={{fontSize:17, fontWeight:'bold', color:colors.secondary}}>Favorite</Text>
          <Icon
            name='ios-heart'
            style={{paddingLeft:10, fontSize:20, color:colors.secondary}}
          />
        </Right>
      </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
        {listFavorite}
      </View>
      <CustomFooter navigation={this.props.navigation} menu="Favorite" />
      </Container>
    );
  }
}
